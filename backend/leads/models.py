from django.db import models

class Lead(models.Model):
    contact = models.CharField(max_length=100)
    user = models.CharField(max_length=100)
    text = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
